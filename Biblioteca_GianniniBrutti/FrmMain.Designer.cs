﻿namespace Biblioteca_GianniniBrutti
{
    partial class frmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvListLibri = new System.Windows.Forms.ListView();
            this.chID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTitolo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chGenere = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAutore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chPosizioneScaffale = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAnnoPubblicazione = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chPrezzo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chQuantita = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btInserisci = new System.Windows.Forms.Button();
            this.btModifica = new System.Windows.Forms.Button();
            this.btCerca = new System.Windows.Forms.Button();
            this.btElimina = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbListaGeneri = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lvListLibri
            // 
            this.lvListLibri.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chID,
            this.chTitolo,
            this.chGenere,
            this.chAutore,
            this.chPosizioneScaffale,
            this.chAnnoPubblicazione,
            this.chPrezzo,
            this.chQuantita});
            this.lvListLibri.HideSelection = false;
            this.lvListLibri.Location = new System.Drawing.Point(16, 106);
            this.lvListLibri.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvListLibri.Name = "lvListLibri";
            this.lvListLibri.Size = new System.Drawing.Size(492, 526);
            this.lvListLibri.TabIndex = 0;
            this.lvListLibri.UseCompatibleStateImageBehavior = false;
            this.lvListLibri.View = System.Windows.Forms.View.Details;
            // 
            // chID
            // 
            this.chID.Text = "ID";
            // 
            // chTitolo
            // 
            this.chTitolo.Text = "Titolo";
            // 
            // chGenere
            // 
            this.chGenere.Text = "Genere";
            // 
            // chAutore
            // 
            this.chAutore.Text = "Autore";
            // 
            // chPosizioneScaffale
            // 
            this.chPosizioneScaffale.Text = "PosizioneScaffale";
            // 
            // chAnnoPubblicazione
            // 
            this.chAnnoPubblicazione.Text = "AnnoPubblicazione";
            // 
            // chPrezzo
            // 
            this.chPrezzo.Text = "Prezzo";
            // 
            // chQuantita
            // 
            this.chQuantita.Text = "Quantita";
            // 
            // btInserisci
            // 
            this.btInserisci.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInserisci.Location = new System.Drawing.Point(516, 141);
            this.btInserisci.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btInserisci.Name = "btInserisci";
            this.btInserisci.Size = new System.Drawing.Size(100, 28);
            this.btInserisci.TabIndex = 1;
            this.btInserisci.Text = "INSERISCI";
            this.btInserisci.UseVisualStyleBackColor = true;
            // 
            // btModifica
            // 
            this.btModifica.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btModifica.Location = new System.Drawing.Point(516, 177);
            this.btModifica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btModifica.Name = "btModifica";
            this.btModifica.Size = new System.Drawing.Size(100, 28);
            this.btModifica.TabIndex = 2;
            this.btModifica.Text = "MODIFICA";
            this.btModifica.UseVisualStyleBackColor = true;
            // 
            // btCerca
            // 
            this.btCerca.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCerca.Location = new System.Drawing.Point(348, 60);
            this.btCerca.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btCerca.Name = "btCerca";
            this.btCerca.Size = new System.Drawing.Size(100, 32);
            this.btCerca.TabIndex = 3;
            this.btCerca.Text = "CERCA";
            this.btCerca.UseVisualStyleBackColor = true;
            // 
            // btElimina
            // 
            this.btElimina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btElimina.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btElimina.Location = new System.Drawing.Point(516, 213);
            this.btElimina.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btElimina.Name = "btElimina";
            this.btElimina.Size = new System.Drawing.Size(100, 28);
            this.btElimina.TabIndex = 4;
            this.btElimina.Text = "ELIMINA";
            this.btElimina.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "cerca per genere";
            // 
            // cbListaGeneri
            // 
            this.cbListaGeneri.FormattingEnabled = true;
            this.cbListaGeneri.Location = new System.Drawing.Point(164, 66);
            this.cbListaGeneri.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbListaGeneri.Name = "cbListaGeneri";
            this.cbListaGeneri.Size = new System.Drawing.Size(160, 24);
            this.cbListaGeneri.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("ROG Fonts", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(80, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(414, 44);
            this.label2.TabIndex = 7;
            this.label2.Text = "CATALOGO LIBRI";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 647);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbListaGeneri);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btElimina);
            this.Controls.Add(this.btCerca);
            this.Controls.Add(this.btModifica);
            this.Controls.Add(this.btInserisci);
            this.Controls.Add(this.lvListLibri);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmMain";
            this.Text = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvListLibri;
        private System.Windows.Forms.Button btInserisci;
        private System.Windows.Forms.Button btModifica;
        private System.Windows.Forms.Button btCerca;
        private System.Windows.Forms.Button btElimina;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbListaGeneri;
        private System.Windows.Forms.ColumnHeader chID;
        private System.Windows.Forms.ColumnHeader chTitolo;
        private System.Windows.Forms.ColumnHeader chGenere;
        private System.Windows.Forms.ColumnHeader chAutore;
        private System.Windows.Forms.ColumnHeader chPosizioneScaffale;
        private System.Windows.Forms.ColumnHeader chAnnoPubblicazione;
        private System.Windows.Forms.ColumnHeader chPrezzo;
        private System.Windows.Forms.ColumnHeader chQuantita;
        private System.Windows.Forms.Label label2;
    }
}

