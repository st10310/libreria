﻿namespace Biblioteca_GianniniBrutti
{
    partial class FrmInserisci
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbIDLibro = new System.Windows.Forms.TextBox();
            this.cbGenere = new System.Windows.Forms.ComboBox();
            this.dtpAnnoPubblicazione = new System.Windows.Forms.DateTimePicker();
            this.nudQuantitaLibro = new System.Windows.Forms.NumericUpDown();
            this.tbTitoloLibro = new System.Windows.Forms.TextBox();
            this.tbNomeAutore = new System.Windows.Forms.TextBox();
            this.nudPrezzoLibro = new System.Windows.Forms.NumericUpDown();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.btInserisci = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantitaLibro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrezzoLibro)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inserisci ID del libro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Inserisci titolo del libro";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Inserisci nome autore";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Inserisci genere del libro";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(186, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Inserisci anno pubblicazione";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(160, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Inserisci prezzo del libro";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Quanti libri?";
            // 
            // tbIDLibro
            // 
            this.tbIDLibro.Location = new System.Drawing.Point(148, 82);
            this.tbIDLibro.Name = "tbIDLibro";
            this.tbIDLibro.Size = new System.Drawing.Size(175, 22);
            this.tbIDLibro.TabIndex = 8;
            // 
            // cbGenere
            // 
            this.cbGenere.FormattingEnabled = true;
            this.cbGenere.Location = new System.Drawing.Point(219, 147);
            this.cbGenere.Name = "cbGenere";
            this.cbGenere.Size = new System.Drawing.Size(121, 24);
            this.cbGenere.TabIndex = 9;
            // 
            // dtpAnnoPubblicazione
            // 
            this.dtpAnnoPubblicazione.Location = new System.Drawing.Point(204, 228);
            this.dtpAnnoPubblicazione.Name = "dtpAnnoPubblicazione";
            this.dtpAnnoPubblicazione.Size = new System.Drawing.Size(200, 22);
            this.dtpAnnoPubblicazione.TabIndex = 10;
            // 
            // nudQuantitaLibro
            // 
            this.nudQuantitaLibro.Location = new System.Drawing.Point(102, 302);
            this.nudQuantitaLibro.Name = "nudQuantitaLibro";
            this.nudQuantitaLibro.Size = new System.Drawing.Size(120, 22);
            this.nudQuantitaLibro.TabIndex = 11;
            // 
            // tbTitoloLibro
            // 
            this.tbTitoloLibro.Location = new System.Drawing.Point(165, 113);
            this.tbTitoloLibro.Name = "tbTitoloLibro";
            this.tbTitoloLibro.Size = new System.Drawing.Size(175, 22);
            this.tbTitoloLibro.TabIndex = 12;
            // 
            // tbNomeAutore
            // 
            this.tbNomeAutore.Location = new System.Drawing.Point(165, 180);
            this.tbNomeAutore.Name = "tbNomeAutore";
            this.tbNomeAutore.Size = new System.Drawing.Size(175, 22);
            this.tbNomeAutore.TabIndex = 14;
            // 
            // nudPrezzoLibro
            // 
            this.nudPrezzoLibro.Location = new System.Drawing.Point(178, 267);
            this.nudPrezzoLibro.Name = "nudPrezzoLibro";
            this.nudPrezzoLibro.Size = new System.Drawing.Size(120, 22);
            this.nudPrezzoLibro.TabIndex = 15;
            // 
            // btAnnulla
            // 
            this.btAnnulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAnnulla.Location = new System.Drawing.Point(12, 415);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(97, 23);
            this.btAnnulla.TabIndex = 16;
            this.btAnnulla.Text = "ANNULLA";
            this.btAnnulla.UseVisualStyleBackColor = true;
            // 
            // btInserisci
            // 
            this.btInserisci.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInserisci.Location = new System.Drawing.Point(402, 415);
            this.btInserisci.Name = "btInserisci";
            this.btInserisci.Size = new System.Drawing.Size(97, 23);
            this.btInserisci.TabIndex = 17;
            this.btInserisci.Text = "INSERISCI";
            this.btInserisci.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Snap ITC", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(49, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(377, 36);
            this.label3.TabIndex = 18;
            this.label3.Text = "INSERISCI UN LIBRO";
            // 
            // FrmInserisci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btInserisci);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.nudPrezzoLibro);
            this.Controls.Add(this.tbNomeAutore);
            this.Controls.Add(this.tbTitoloLibro);
            this.Controls.Add(this.nudQuantitaLibro);
            this.Controls.Add(this.dtpAnnoPubblicazione);
            this.Controls.Add(this.cbGenere);
            this.Controls.Add(this.tbIDLibro);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmInserisci";
            this.Text = "FrmInserisci";
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantitaLibro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrezzoLibro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbIDLibro;
        private System.Windows.Forms.ComboBox cbGenere;
        private System.Windows.Forms.DateTimePicker dtpAnnoPubblicazione;
        private System.Windows.Forms.NumericUpDown nudQuantitaLibro;
        private System.Windows.Forms.TextBox tbTitoloLibro;
        private System.Windows.Forms.TextBox tbNomeAutore;
        private System.Windows.Forms.NumericUpDown nudPrezzoLibro;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.Button btInserisci;
        private System.Windows.Forms.Label label3;
    }
}