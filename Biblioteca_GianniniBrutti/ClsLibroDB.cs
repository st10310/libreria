﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca_GianniniBrutti
{
    public class ClsLibroDB
    {
        string _titolo, _genere, _ID, _posizioneScaffale, _autore;
        DateTime _annoPubblicazione;
        public string Titolo
        {
            //lettura del valore
            get { return _titolo; }
            //scrittura del valore
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) _titolo = value.Trim(); //nome contenuto in value
                else throw new Exception("Il titolo non può essere di 0 caratteri");
            }
        }
        public string Genere
        {
            //lettura del valore
            get { return _genere; }
            //scrittura del valore
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) _genere = value.Trim(); //cognome contenuto in value
                else throw new Exception("Il genere non può essere di 0 caratteri");
            }
        }
        public string ID
        {
            //lettura del valore
            get { return _ID; }
            //scrittura del valore
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) _ID = value.Trim(); //matricola contenuta in value
                else throw new Exception("L'ID non può essere di 0 caratteri");
            }
        }
        public string PosizioneScaffale
        {
            //lettura del valore
            get { return _posizioneScaffale; }
            //scrittura del valore
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) _posizioneScaffale = value.Trim(); //matricola contenuta in value
                else throw new Exception("La posizione nello scaffale non può essere di 0 caratteri");
            }
        }
        public string Autore
        {
            //lettura del valore
            get { return _autore; }
            //scrittura del valore
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) _autore = value.Trim(); //matricola contenuta in value
                else throw new Exception("L'autore non può essere di 0 caratteri");
            }
        }
        public DateTime AnnoPubblicazione
        {
            //lettura del valore
            get { return _annoPubblicazione; }
            //scrittura del valore
            set
            {
                _annoPubblicazione = value;
            }
        }
        public ClsLibroDB()
        {

        }
    }
}
